#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <linux/types.h>
#include <sqlite3.h>
#include <ldns/ldns.h>
#include <limits.h>
#include <asm/byteorder.h>

#define ARRAY_SIZE(x)		(sizeof(x) / sizeof(*(x)))
typedef unsigned int u32;
#if 0
CREATE TABLE records (
		id              INTEGER PRIMARY KEY,
		domain_id       INTEGER DEFAULT NULL,
		name            VARCHAR(255) DEFAULT NULL,
		type            VARCHAR(6) DEFAULT NULL,
		content         VARCHAR(255) DEFAULT NULL,
		ttl             INTEGER DEFAULT NULL,
		prio            INTEGER DEFAULT NULL,
		change_date     INTEGER DEFAULT NULL
);
#endif

struct dns_record {
	u32 id;
	u32 domain_id;
	char *name;
	char *type;
	char *content;
	u32 ttl;
	u32 prio;
	u32 change_data;
	ldns_rr *rr;
};

static sqlite3 *sqdb;
static sqlite3_stmt *entry_stmt;
static sqlite3_stmt *count_stmt;
static sqlite3_stmt *del_stmt;
static sqlite3_stmt *add_stmt;
static sqlite3_stmt *gid_stmt;

static int init_sqlite(const char *dbname)
{
	u32 ret;

	ret = sqlite3_open_v2(dbname, &sqdb, SQLITE_OPEN_READWRITE, NULL);
	if (ret) {
		fprintf(stderr, "Failed to open: %s: %m\n", dbname);
		return 1;
	}
	ret = sqlite3_prepare(sqdb,
			"select * from records where domain_id = ?1;", -1,
			&entry_stmt, NULL);
	if (ret) {
		fprintf(stderr, "Failed on prepare statement\n");
		return 1;
	}
	ret = sqlite3_prepare(sqdb,
			"select count(*) from records where domain_id = ?1;",
			-1, &count_stmt, NULL);
	if (ret) {
		fprintf(stderr, "Failed on prepare statement\n");
		return 1;
	}
	ret = sqlite3_prepare(sqdb,
			"delete from records where id = ?1;",
			-1, &del_stmt, NULL);
	if (ret) {
		fprintf(stderr, "Failed on prepare statement\n");
		return 1;
	}

	ret = sqlite3_prepare(sqdb,
			"insert into records (domain_id, name, type, content, ttl, prio) "
			"values (?1, ?2, ?3, ?4, ?5, ?6);",
			-1, &add_stmt, NULL);
	if (ret) {
		fprintf(stderr, "Failed on prepare statement\n");
		return 1;
	}
	ret = sqlite3_prepare(sqdb, "select id from domains where name = ?1;",
			-1, &gid_stmt, NULL);
	if (ret) {
		fprintf(stderr, "Failed on prepare statement\n");
		return 1;
	}
	return 0;
}

static void finish_sqlite(void)
{
	sqlite3_finalize(gid_stmt);
	sqlite3_finalize(add_stmt);
	sqlite3_finalize(del_stmt);
	sqlite3_finalize(count_stmt);
	sqlite3_finalize(entry_stmt);
	sqlite3_close(sqdb);
}

static int sq_get_domain_by_id(u32 id, struct dns_record **rdns, u32 *rentr)
{
	struct dns_record *dns;
	u32 entries;
	int ret;
	u32 i;

	ret = sqlite3_bind_int(count_stmt, 1, id);
	if (ret) {
		fprintf(stderr, "Failed to bind\n");
		return 1;
	}
	ret = sqlite3_step(count_stmt);
	if (ret != SQLITE_ROW) {
		fprintf(stderr, "Count failed: %d\n", ret);
		return 1;
	}
	entries = sqlite3_column_int(count_stmt, 0);
	ret = sqlite3_reset(count_stmt);
	if (ret) {
		fprintf(stderr, "reset failed: %d\n", ret);
		return 1;
	}
	sqlite3_clear_bindings(count_stmt);
	if (ret) {
		fprintf(stderr, "clear binding failed: %d\n", ret);
		return 1;
	}

	dns = malloc(sizeof(struct dns_record) * entries);
	if (!dns) {
		printf("OOM\n");
		return 1;
	}

	ret = sqlite3_bind_int(entry_stmt, 1, id);
	if (ret) {
		fprintf(stderr, "Failed to bind\n");
		goto err_free_dns;
	}

	i = 0;
	do {
		ret = sqlite3_step(entry_stmt);
		if (ret == SQLITE_DONE) {
			break;
		}

		if (i >= entries) {
			printf("too much sql entries.\n");
			goto err_reset;
		}

		if (ret == SQLITE_ROW) {
			dns[i].id = sqlite3_column_int(entry_stmt, 0);
			dns[i].domain_id = sqlite3_column_int(entry_stmt, 1);

			dns[i].name = strdup((const char*)
					sqlite3_column_text(entry_stmt, 2));
			dns[i].type = strdup((const char*)
					sqlite3_column_text(entry_stmt, 3));
			dns[i].content = strdup((const char*)
					sqlite3_column_text(entry_stmt, 4));

			dns[i].ttl = sqlite3_column_int(entry_stmt, 5);
			dns[i].prio = sqlite3_column_int(entry_stmt, 6);
			dns[i].change_data = sqlite3_column_int(entry_stmt, 7);

			i++;
		}
	} while (1);
	if (i != entries) {
		printf("Wrong number of sql rows. Expected %d got %d\n",
				entries, i);
		goto err_reset;
	}
	*rdns = dns;
	*rentr = entries;

	sqlite3_reset(entry_stmt);
	sqlite3_clear_bindings(entry_stmt);
	return 0;

err_reset:
	sqlite3_reset(entry_stmt);
	sqlite3_clear_bindings(entry_stmt);
err_free_dns:
	free(dns);
	return 1;
}

static ldns_zone *record_to_zone(struct dns_record *dns, u32 entries)
{
	ldns_zone *zone;
	ldns_rdf *prev = NULL;
	char opt_prio[10];
	char dns_buf[1024];
	u32 i;

	zone = ldns_zone_new();
	for (i = 0; i < entries; i++) {
		ldns_rr *rr;
		ldns_status status;

		if (!strcmp(dns[i].type, "MX"))
			snprintf(opt_prio, sizeof(opt_prio), "%d ",
					dns[i].prio);
		else
			opt_prio[0] = '\0';
		snprintf(dns_buf, sizeof(dns_buf), "%s %d IN %s %s%s",
				dns[i].name,
				dns[i].ttl,
				dns[i].type,
				opt_prio,
				dns[i].content);
		status = ldns_rr_new_frm_str(&rr, dns_buf, 0, NULL, &prev);
		if (status == LDNS_STATUS_OK) {

			if (ldns_rr_get_type(rr) == LDNS_RR_TYPE_SOA) {
				if (!ldns_zone_soa(zone)) {
					ldns_zone_set_soa(zone, rr);
					dns[i].rr = rr;
				} else {
					printf("Found another SOA. Dropping.\n");
					ldns_rr_free(rr);
				}
			} else {
				bool b;

				b = ldns_zone_push_rr(zone, rr);
				if (b == false) {
					printf("Failed to push rr\n");
				} else
					dns[i].rr = rr;
			}
		} else
			printf("Error %d parsing '%s'\n=> %s\n", status, dns_buf,
					ldns_get_errorstr_by_id(status));
	}
	return zone;
}

static ldns_zone *zone_from_file(const char *fname)
{
	ldns_status s;
	ldns_zone *zone;
	FILE *f;
	int line = 0;

	f = fopen(fname, "r");
	if (!f) {
		printf("Failed to open '%s': %m\n", fname);
		return NULL;
	}
	s = ldns_zone_new_frm_fp_l(&zone, f, NULL, 0, LDNS_RR_CLASS_IN, &line);
	fclose(f);
	if (s == LDNS_STATUS_OK)
		return zone;
	printf("Error '%s' in %s:%d\n", ldns_get_errorstr_by_id(s), fname,
			line);
	return NULL;
}

static void compare_zones(ldns_zone *old_z, ldns_zone *new_z,
		ldns_rr_list *rr_rm, ldns_rr_list *rr_add)
{
	ldns_rr_list *old_rl;
	ldns_rr_list *new_rl;
	u32 old_p = 0;
	u32 new_p = 0;

	ldns_zone_sort(old_z);
	ldns_zone_sort(new_z);

	old_rl = ldns_zone_rrs(old_z);
	new_rl = ldns_zone_rrs(new_z);

	while (1) {
		int ret;
		ldns_rr *old_rr;
		ldns_rr *new_rr;

		old_rr = ldns_rr_list_rr(old_rl, old_p);
		new_rr = ldns_rr_list_rr(new_rl, new_p);

		if (old_rr && new_rr)
			ret = ldns_rr_compare(old_rr, new_rr);

		else if (!old_rr && !new_rr)
			break;

		else if (old_rr && !new_rr)
			ret = -1;

		else /* if (!old_rr && new_rr) */
			ret = 1;

		if (!ret) {
			/* check TTL */
			if (ldns_rr_ttl(old_rr) != ldns_rr_ttl(new_rr)) {
				ldns_rr_list_push_rr(rr_rm, old_rr);
				ldns_rr_list_push_rr(rr_add, new_rr);
			}
			/* ret = 0 */
		}
		if (!ret) {
			old_p++;
			new_p++;
		} else if  (ret < 0) {
			old_p++;

			ldns_rr_list_push_rr(rr_rm, old_rr);

		} else /* if (ret > 0) */ {
			new_p++;

			ldns_rr_list_push_rr(rr_add, new_rr);
		}
	}
}

static int sql_delete(u32 id)
{
	int ret;

	ret = sqlite3_bind_int(del_stmt, 1, id);
	if (ret) {
		fprintf(stderr, "Failed to bind (del)\n");
		return -1;
	}
	ret = sqlite3_step(del_stmt);
	if (ret != SQLITE_DONE) {
		fprintf(stderr, "delete failed: %d\n", ret);
		return -1;
	}
	sqlite3_reset(del_stmt);
	sqlite3_clear_bindings(del_stmt);
	return 0;
}

static int purge_rr(struct dns_record *dns, u32 entr, ldns_rr *rr)
{
	u32 i;

	for (i = 0; i < entr; i++) {
		if (dns[i].rr == rr)
			return sql_delete(dns[i].id);
	}
	printf("Entry not found\n");
	return -1;
}

static int remove_sq_zones(ldns_rr_list *rr_rm, struct dns_record *dns,
		u32 entr)
{
	ldns_rr *rr;
	int ret;

	do {
		rr = ldns_rr_list_pop_rr(rr_rm);
		if (!rr)
			break;

		ret = purge_rr(dns, entr, rr);
		if (ret)
			return ret;

	} while (1);

	return 0;
}

static char *get_name_from_rr(ldns_rr *rr)
{
	char *str;
	ldns_buffer *buff;
	ldns_status st;

	buff = ldns_buffer_new(LDNS_MAX_PACKETLEN);
	if (!buff)
		return NULL;

	st = ldns_rdf2buffer_str_dname(buff, ldns_rr_owner(rr));
	if (st != LDNS_STATUS_OK) {
		ldns_buffer_free(buff);
		return NULL;
	}
	str = ldns_buffer2str(buff);
	ldns_buffer_free(buff);

	return str;
}

static char *get_type_from_rr(ldns_rr *rr)
{
	char *str;
	ldns_buffer *buff;
	ldns_status st;

	buff = ldns_buffer_new(LDNS_MAX_PACKETLEN);
	if (!buff)
		return NULL;

	st = ldns_rr_type2buffer_str(buff, ldns_rr_get_type(rr));
	if (st != LDNS_STATUS_OK) {
		ldns_buffer_free(buff);
		return NULL;
	}
	str = ldns_buffer2str(buff);
	ldns_buffer_free(buff);

	return str;
}

static char *get_str_from_rr(ldns_rr *rr, int index)
{
	ldns_buffer *buff;
	ldns_status st;
	char *str;

	buff = ldns_buffer_new(LDNS_MAX_PACKETLEN);
	if (!buff)
		return NULL;

	if (index >= 0) {
		st = ldns_rdf2buffer_str(buff, ldns_rr_rdf(rr, index));
		if (st != LDNS_STATUS_OK)
			goto err;
	} else {
		u32 entries;
		u32 i;

		entries = ldns_rr_rd_count(rr);
		for (i = 0; i < entries; i++) {
			st = ldns_rdf2buffer_str(buff, ldns_rr_rdf(rr, i));
			if (st != LDNS_STATUS_OK)
				goto err;
			if (i < entries - 1)
				ldns_buffer_printf(buff, " ");
		}
	}
	str = ldns_buffer2str(buff);
	ldns_buffer_free(buff);

	return str;
err:
	ldns_buffer_free(buff);
	return NULL;
}

static char *get_content_from_rr(ldns_rr *rr, u32 *prio)
{
	char *str;
	ldns_rr_type type;
	u32 count;
	u32 need_count;

	type = ldns_rr_get_type(rr);
	switch (type) {
	case LDNS_RR_TYPE_MX:
		need_count = 2;
		break;
	case LDNS_RR_TYPE_SOA:
		need_count = 7;
		break;
	default:
		need_count = 1;
		break;
	}
	count = ldns_rr_rd_count(rr);
	if (need_count != count) {
		fprintf(stderr, "Record type %d has unknown number of records.\n",
				type);
		fprintf(stderr, "Found: %d, expected %d\nContent: ", count, need_count);
		ldns_rr_print(stderr, rr);
		return NULL;
	}

	if (type == LDNS_RR_TYPE_MX) {
		char *endp;
		long mx_prio;

		str = get_str_from_rr(rr, 0);
		if (!str)
			return NULL;
		mx_prio = strtol(str, &endp, 10);
		free(str);

		if (mx_prio < 0)
			return NULL;
		if (mx_prio > INT_MAX)
			return NULL;
		if (endp == str)
			return NULL;

		str = get_str_from_rr(rr, 1);
		if (!str)
			return NULL;
		*prio = mx_prio;
	} else {
		str = get_str_from_rr(rr, -1);
		*prio = 0;
	}

	return str;
}

static int add_rr(ldns_rr *rr, int dom_id)
{
	int ret;
	char *name;
	char *type;
	char *content;
	u32 ttl;
	u32 prio = 0;

	name = get_name_from_rr(rr);
	type = get_type_from_rr(rr);
	content = get_content_from_rr(rr, &prio);

	if (!name || !type || !content) {
		ret = -1;
		goto out;
	}

	ttl = ldns_rr_ttl(rr);

	sqlite3_bind_int(add_stmt, 1, dom_id);
	sqlite3_bind_text(add_stmt, 2, name, -1, SQLITE_STATIC);
	sqlite3_bind_text(add_stmt, 3, type, -1, SQLITE_STATIC);
	sqlite3_bind_text(add_stmt, 4, content, -1, SQLITE_STATIC);
	sqlite3_bind_int(add_stmt, 5, ttl);
	sqlite3_bind_int(add_stmt, 6, prio);

	ret = sqlite3_step(add_stmt);
	if (ret != SQLITE_DONE) {
		fprintf(stderr, "insert failed: %d\n", ret);
		ret = -1;
		goto err;
	}
	ret = 0;
err:
	sqlite3_reset(add_stmt);
	sqlite3_clear_bindings(add_stmt);
out:
	free(name);
	free(type);
	free(content);

	return ret;
}

static int add_sq_zones(ldns_rr_list *rrl, int dom_id)
{
	ldns_rr *rr;
	int ret;

	do {
		rr = ldns_rr_list_pop_rr(rrl);
		if (!rr)
			break;

		ret = add_rr(rr, dom_id);
		if (ret)
			return -1;

	} while (1);

	return 0;
}

static int sq_get_id(const char *name)
{
	int id;
	int ret;

	ret = sqlite3_bind_text(gid_stmt, 1, name, -1, SQLITE_STATIC);
	if (ret) {
		fprintf(stderr, "Failed to bind\n");
		return -1;
	}
	ret = sqlite3_step(gid_stmt);
	if (ret != SQLITE_ROW) {
		fprintf(stderr, "Select failed: %d\n", ret);
		return -1;
	}
	id = sqlite3_column_int(gid_stmt, 0);
	ret = sqlite3_reset(gid_stmt);
	if (ret) {
		fprintf(stderr, "reset failed: %d\n", ret);
		return -1;
	}
	sqlite3_clear_bindings(gid_stmt);
	if (ret) {
		fprintf(stderr, "clear binding failed: %d\n", ret);
		return -1;
	}
	return id;
}

static char *get_zname(ldns_rr *rr)
{
	int len;
	char *zname;

	zname = get_name_from_rr(rr);
	if (!zname) {
		fprintf(stderr, "Can't domain name from SOA\n");
		return NULL;
	}

	len = strlen(zname);
	if (!len) {
		free(zname);
		return NULL;
	}
	if (zname[len - 1] == '.')
		zname[len - 1] = '\0';
	return zname;
}

static int compare_dname(ldns_rr *old_rr, ldns_rr *new_rr, u32 num)
{
	char *old_str;
	char *new_str;
	int ret = -1;

	old_str = get_str_from_rr(old_rr, num);
	new_str = get_str_from_rr(new_rr, num);
	if (!old_str || !new_str)
		goto out;

	if (strcmp(old_str, new_str))
		ret = 1;
	else
		ret = 0;
out:
	free(old_str);
	free(new_str);
	return ret;
}

static int compare_soa(ldns_zone *old_z, ldns_zone *new_z,
		ldns_rr_list *rr_rm, ldns_rr_list *rr_add)
{
	u32 i;
	u32 take_new = 0;
	u32 autogen_serial = 0;
	ldns_rr *old_rr;
	ldns_rr *new_rr;

	old_rr = ldns_zone_soa(old_z);
	new_rr = ldns_zone_soa(new_z);
	if (!old_rr || !old_rr)
		return -1;
	if (ldns_rr_rd_count(old_rr) != ldns_rr_rd_count(new_rr))
		return -1;
	if (ldns_rr_rd_count(old_rr) != 7)
		return -1;

	/* Sanity check of RDF types */
	for (i = 0; i < ldns_rr_rd_count(new_rr); i++) {
		ldns_rdf_type type1;
		ldns_rdf_type type2;
		u32 wrong_format = 0;

		switch (i) {
		case 0:
		case 1:
			type1 = ldns_rdf_get_type(ldns_rr_rdf(old_rr, i));
			type2 = ldns_rdf_get_type(ldns_rr_rdf(new_rr, i));
			if (type1 != LDNS_RDF_TYPE_DNAME || type1 != type2)
				wrong_format = 1;
			break;
		default:
			if (ldns_rdf_size(ldns_rr_rdf(old_rr, i)) != 4)
				wrong_format = 1;
			break;
		}
		if (wrong_format) {
			printf("RDF entry %d of SOA has wrong format.\n", i);
			return -1;
		}
	}

	/* check if a field updated */
	if (ldns_rr_ttl(old_rr) != ldns_rr_ttl(new_rr))
		take_new = 1;
	else if (ldns_rr_list_rr_count(rr_rm) || ldns_rr_list_rr_count(rr_add))
		take_new = 1;

	if (!ldns_rdf2native_int32(ldns_rr_rdf(new_rr, 2)))
		autogen_serial = 1;

	for (i = 0; i < ldns_rr_rd_count(new_rr) && !take_new; i++) {
		int ret;
		u32 old_num;
		u32 new_num;

		switch (i) {
		/* DNAME */
		case 0:
		case 1:
			ret = compare_dname(old_rr, new_rr, i);
			if (ret)
				take_new = 1;
			break;
		/* serial */
		case 2:
			if (autogen_serial)
				break;
			/* fall through */
		/* remaining (numeric) fields */
		default:
			old_num = ldns_rdf2native_int32(ldns_rr_rdf(old_rr, i));
			new_num = ldns_rdf2native_int32(ldns_rr_rdf(new_rr, i));
			take_new = old_num != new_num;
			break;
		}
	}

	if (!take_new)
		return 0;
	/* We have to update the SOA record */

	if (autogen_serial) {
		u32 new_serial;
		u32 *dta_serial;

		/*
		 * RFC 1035, 3.3.13 says serial is an unsigned 32bit value so
		 * this wraps after I'm dead. No need to worry.
		 */
		new_serial = time(NULL);
		dta_serial = (u32 *)ldns_rdf_data(ldns_rr_rdf(new_rr, 2));
		/*
		 * It could be checked here if those are the same but it is
		 * unlikely to happen
		 */
		*dta_serial = __cpu_to_be32(new_serial);
	} else {
		u32 *old_serial;
		u32 *new_serial;

		asm volatile("nop; nop; nop;");
		old_serial = (u32 *)ldns_rdf_data(ldns_rr_rdf(old_rr, 2));
		new_serial = (u32 *)ldns_rdf_data(ldns_rr_rdf(new_rr, 2));

		if (*old_serial == *new_serial) {
			fprintf(stderr, "Serial unchanged. Not updating ");
			ldns_rdf_print(stderr, ldns_rr_owner(new_rr));
			fprintf(stderr, "\n");
			return -1;
		}
	}

	ldns_rr_list_push_rr(rr_rm, old_rr);
	ldns_rr_list_push_rr(rr_add, new_rr);
	return 0;
}

static int compare_zone_and_update_sq(const char *zone_fname)
{
	ldns_zone *f_zone = NULL;
	ldns_zone *sq_zone = NULL;
	ldns_rr *r;
	ldns_rr_list *rr_rm;
	ldns_rr_list *rr_add;
	int domain_id;
	char *zname;
	int ret = -1;
	struct dns_record *dns;
	u32 entries;

	rr_rm = ldns_rr_list_new();
	rr_add = ldns_rr_list_new();
	if (!rr_rm || !rr_add)
		goto cleanup_ret;

	f_zone = zone_from_file(zone_fname);
	if (!f_zone)
		goto cleanup_ret;

	r = ldns_zone_soa(f_zone);
	if (!r) {
		fprintf(stderr, "Missing SOA record\n");
		goto cleanup_ret;
	}

	zname = get_zname(r);
	if (!zname) {
		fprintf(stderr, "Can't domain name from SOA\n");
		goto cleanup_ret;
	}

	domain_id = sq_get_id(zname);
	if (domain_id < 0) {
		fprintf(stderr, "Failed to obtain ID for %s\n", zname);
		goto cleanup_ret;
	}

	ret = sq_get_domain_by_id(domain_id, &dns, &entries);
	if (ret)
		goto cleanup_ret;

	sq_zone = record_to_zone(dns, entries);
	if (!sq_zone) {
		ret = -1;
		goto cleanup_ret;
	}

	compare_zones(sq_zone, f_zone, rr_rm, rr_add);
	ret = compare_soa(sq_zone, f_zone, rr_rm, rr_add);
	if (ret)
		goto cleanup_ret;

	fprintf(stdout, "remove entries:\n");
	ldns_rr_list_print(stdout, rr_rm);
	fprintf(stdout, "\nadd entries:\n");
	ldns_rr_list_print(stdout, rr_add);
	fprintf(stdout, "\n");

	ret = remove_sq_zones(rr_rm, dns, entries);
	if (!ret)
		ret = add_sq_zones(rr_add, domain_id);

cleanup_ret:
	ldns_rr_list_free(rr_rm);
	ldns_rr_list_free(rr_add);
	if (sq_zone)
		ldns_zone_deep_free(sq_zone);
	if (f_zone)
		ldns_zone_deep_free(f_zone);
	return ret;
}

#define OPT_ARGS	"s:z:"

static void show_help(void)
{
	printf(
			"usage: bazoom -s file -z file\n"
			"\t-s\tName of the sqlitedb file\n"
			"\t-z\tName of the zone file\n"
		);
	exit(2);
}

int main(int argc, char **argv)
{
	int ret;
	int c;
	char *sqlite_fname = NULL;
	char *zone_fname = NULL;

	while ((c = getopt (argc, argv, OPT_ARGS)) != -1) {
		switch (c) {
		case 's':
			sqlite_fname = optarg;
			break;
		case 'z':
			zone_fname = optarg;
			break;
		default:
			show_help();
		}
	}

	if (!sqlite_fname) {
		printf("Missing sqlite db file\n");
		show_help();
	}
	if (!zone_fname) {
		printf("Missing zone file\n");
		show_help();
	}

	ret = init_sqlite(sqlite_fname);
	if (ret)
		return 1;

	ret = sqlite3_exec(sqdb, "begin transaction;", NULL, NULL, NULL);
	if (ret) {
		printf("failed to start sql transaction\n");
		ret = 1;
		goto cleanup_ret;
	}

	ret = compare_zone_and_update_sq(zone_fname);
	if (ret) {
		fprintf(stderr, "Not committing changes due to errors.\n");
		sqlite3_exec(sqdb, "rollback transaction;", NULL, NULL, NULL);
		goto cleanup_ret;
	}

	ret = sqlite3_exec(sqdb, "end transaction;", NULL, NULL, NULL);
	if (ret) {
		printf("failed to commit sql transaction\n");
		ret = 1;
		goto cleanup_ret;
	}

	ret = 0;

cleanup_ret:

	finish_sqlite();
	return ret;
}
